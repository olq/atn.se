//https://codepen.io/Siyanda/pen/VLdvRg

$(function(){
	
	// first script to pull posts
  var colors = [ "#FDF311", "#FF8A11", "#FF3340", "#FF008D", "#008AB2", "#00DC28", "#7D19B1", "#EA009C" ];
  var counter = 0;
  var accessToken = '207607829.63a6ea2.23c03682925a42b4875283c85096b472'; // use your own token
  // atn id 4236768222
  //https://api.instagram.com/v1/users/self/media/recent/?access_token=
  $.getJSON('https://api.instagram.com/v1/users/4236768222/media/recent/?access_token='+accessToken+'&count=6',function (insta) {
    $.each(insta.data,function (photos,src) {
      if ( photos === 20 ) { return false; }
      if ( counter == colors.length ) { counter = 0; }
      var textcolor = "#000";

      // Counting the perceptive luminance - human eye favors green color... 
      var luminance = 1 - ( 0.299 * parseInt("0x" + colors[counter].substr(1,2)) + 0.587 * parseInt("0x" +  colors[counter].substr(3,2)) + 0.114 * parseInt("0x" +  colors[counter].substr(5,2)))/255;

      if (luminance < 0.3)
        textcolor = "#000"; // bright colors - black font
      else
        textcolor = "#fff"; // dark colors - white font
      //var date = new Date(this.created_time * 1000);
      //if(this.caption === null){ 
        $( '<a href="' + this.link + '" target="_blank" rel="noopener">'
        + '<img src=" ' + this.images.low_resolution.url + '">'
        + '<div class="instagram-hover" style="background: ' + colors[counter] + 'd6' + ';" >'
        + '<div class="instagram-hover-content">'
        + '<p style="color: ' + textcolor + ';">' + this.caption.text + '</p>'
        + '<span class="fa fa-arrow-right"></span>'
        + '</div>'
        + '</div>'
        + '</img>'
        + '</a>'
        ).appendTo('#instafeed');
        counter++;
      /* }
      else {
        $( '<a target="_blank" href="' + this.link + '"> '
        + '<img src=" '
        + this.images.low_resolution.url + '" />'
        //+ '<p>' + this.caption.text + '</p>'
        + '</a>'
        ).appendTo('#instafeed');
      } */
      
    }); 
  });

});
