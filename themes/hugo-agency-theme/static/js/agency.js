// Smooth scrolling via animate()
$(document).ready(function(){
  $("a.page-scroll").on('click', function(event) {
    if (this.hash && window.location.pathname == "/") {
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function(){
        window.location.hash = hash;
      });
    }
  });
});


// Navigation change on scroll
$(document).ready(function(){
  var maxOffset = 300;
  $(window).scroll(function() {
    if ($(window).scrollTop() >= maxOffset) {
      $('.navbar-default').addClass('navbar-shrink');
      // $('.logo-white').addClass('logo-hide');
      // document.getElementById('.logo-white').style.display = 'none';
      // document.getElementById('logo-black').style.display = 'block';
    }
    else {
      // $('.logo-white').removeClass('logo-hide');
      $('.navbar-default').removeClass('navbar-shrink');
      // document.getElementById('.logo-white').style.display = 'block';
      // document.getElementById('logo-black').style.display = 'none';
    }
  });
});

$(document).ready(function(){
  var maxOffset = 300;
  if ($(window).scrollTop() >= maxOffset) {
    $('.navbar-default').addClass('navbar-shrink');
    // $('.logo-white').addClass('logo-hide');
    // document.getElementById('.logo-white').style.display = 'none';
    // document.getElementById('logo-black').style.display = 'block';
  }
  else {
    $('.navbar-default').removeClass('navbar-shrink');
    // $('.logo-white').removeClass('logo-hide');
    // document.getElementById('.logo-white').style.display = 'block';
    // document.getElementById('logo-black').style.display = 'none';
  }
});


// Highlight the top nav as scrolling occurs
// $('body').scrollspy({
//     target: '.navbar-fixed-top'
// })

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function() {
    $('.navbar-toggle:visible').click();
});

// Async contact form
/* $('form[id=contactForm]').submit(function(){
  $.post($(this).attr('action'), $(this).serialize(), function(data, textStatus, jqXHR){
    $('form[id=contactForm] #success').hide();
    $('form[id=contactForm] #error').hide();
    if (jqXHR.status == 200) {
      $('form[id=contactForm] #success').show();
    }}, 'json').fail(function(){
      $('form[id=contactForm] #success').hide();
      $('form[id=contactForm] #error').hide();
      $('form[id=contactForm] #error').show();
  });
  return false;
}); */

// Contact form validation
/* $.validate({
  modules : 'html5, toggleDisabled'
}); */

// tooltip oscar

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});

//portfolio previous/next buttons
$('#myTabs a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})

$('.modal').on("show.bs.modal", function () {
  $('.lazy_load').each(function(){
      var img = $(this);
      img.attr('src', img.data('src'));
  });
});     

$(document).ready(function(){
  // PMS 803 804 805 806 801-2x 802-2x 814-2x
  // gul orange röd rosa blå grön lila
  var colors = [ "#FDF311", "#FF8A11", "#FF3340", "#FF008D", "#008AB2", "#00DC28", "#7D19B1", "#EA009C" ];
  var color_counter = 0;

  var portfolio_objects = document.querySelectorAll("#portfolio .portfolio-item .portfolio-link .portfolio-hover");
  var textcolor = "#000";
  // Counting the perceptive luminance - human eye favors green color... 

  for(i = 0; i < portfolio_objects.length; i++){
    if(color_counter === colors.length - 1) { color_counter = 0;}
    portfolio_objects[i].style.background =  colors[color_counter] + "d6"; // d6 == transparency

    var luminance = 1 - ( 0.299 * parseInt("0x" + colors[color_counter].substr(1,2)) + 0.587 * parseInt("0x" +  colors[color_counter].substr(3,2)) + 0.114 * parseInt("0x" +  colors[color_counter].substr(5,2)))/255;
    if (luminance < 0.3)
       textcolor = "#555"; // bright colors - black font
    else
      textcolor = "#fff"; // dark colors - white font
    portfolio_objects[i].style.color = textcolor;


    color_counter++;
  }
});