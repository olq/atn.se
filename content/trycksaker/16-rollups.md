---
title: Rollups MD
subtitle: Stora och fina rollups till mässan
date: 2018-01-16
# img: annonser.jpg
# preview: thumbs/annonser400.jpg
img: kek3.jpg
preview: kek3.jpg
client: design
category: design
description: Trycks på tygliknande material, sätts i medföljande kassetter, eller använd egna.
modalID: 16
prev: portfolioModal-kontorstrycksaker
next: portfolioModal-grafisk-atelje
color: "#86A42A"
---

# yao
hanna är söt