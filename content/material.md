+++
title = "Lämna material"
lastmod = "2017-10-05"
date = "2017-10-05"
subtitle = "Lite om filformat och hur man skickar filerna"
type = "lamnamaterial"
+++

## SKICKA FILER

Om du vill föra över digitalt material till Affärstryckeriet utan att behöva komma förbi
med ett USB minne är det smidigaste och säkraste
sättet att sicka filerna via vår molntjänst. Du får då en unik länk till vår molntjänst skickat till dig via e-post. Där kan du enkelt lägga in filer via webläsaren. När du gjort det så får vi en notis om att filerna dykt upp i din mapp.

Att skicka filer via e-mail går bra så länge filerna inte är för stora eller kräver specifik sekretess. Använd <a mailto:info@atn.se>info@atn.se</a> för att skicka direkt till grafikerna.

Har du möjlighet att göra filerna tillgängliga för oss via egna molntjänster, tex Dropbox, så går det utmärkt att skicka länken till <a mailto:info@atn.se>info@atn.se</a>.


## LÄMNA TEXT

Ska du lämna in text som vi sedan ska arbeta med? Det går bra att lämna text på vilket sätt du vill.
Vi tar ut texten oformaterad och ”startar” gör om layout från början så att det ser bra ut på din trycksak. Det absolut simplaste är att lämna direkt i ett mail eller en textfil.

## KORREKTUR

Vi skickar alltid ett lågupplöst korrektur via mailen när formgivningen är klar. Om så önskas kan du även få på provtryck. Det är mycket viktigt att du är nöjd med slutresultatet.

Läs noggrant igenom korrekturet och kontrollera allt flera gånger så som stavning, adresser, bilder och färger.
Vid godkänt korrektur tar vi inget ansvar för innehållsmässiga fel som upptäcks efter tryck.

## BILD

Spara dina bilder i RGB eller CMYK. Bilder bör ha en upplösning mellan 150 och 300 dpi, beroende hur nära man kommer beskåda trycksaken i verkligheten. Vi kontrollerar att dina bilder och grafik har rätt format och kvalitet så gott vi kan.

Överkurs: Våra maskiner är kallibrerade med färgprofiler i ISO####-skalan. Oftast är det inget du som kund behöver tänka på, men om originalet är framställt på en reklambyrå kan det vara en bra idé att tänka på det och göra oss uppmärksammade om. Ett provtryck från ett annat tryckeri eller reklambyrå kan komma att se lite annorlunda ut än vårt tryck om vi inte haft möjlighet att se deras provtryck i förväg. 

## ILLUSTRATIONER

Med vektorgrafik kan bilder göras hur stora som helst. För bästa resultat bör logotyper och vektorgrafik vara sparade i AI, SVG, EPS eller andra format som stöds av Adobe Illustrator. Dekorfärger definieras alltid enligt Pantoneskalan. Konvertera eventuell text till textkonturer eller banor.

Illustrationer som sparats om till vanliga bildformat bör ha en upplösning på 300-600 ppi. 

## LAYOUTPROGRAM & PROGRAMVAROR

Vanliga kontorsprogram så som Word och Powerpoint, klarar sällan att skapa ett korrekt och snyggt original. De har dålig typografisk kontroll, de kan flöda om text när dokument flyttas till en annan dator och ändra marginalerna i dokumentet beroende på vilken skrivare som används. Men om du måste, så spara som PDF.

## PACKA FILER

Om du lämnar ett ”öppet dokument” så se till att du skickar med länkar och typsnitt. I de flesta layoutprogram kan man packa ihop alla filer och teckensnitt. InDesign tex. Arkiv – Packa.

## UTFALL (bleed)

Vill du ha bilder eller bakgrund ut till kanten? Lägg då utfallande objekt minst 2 mm utanför dokumentformatet så att vi har en skärmån. Om inte utfall finns behöver vi förstora bilden något.

## SKÄRMÄRKEN

Vi tar helst emot original utan skärmärken. Om ni själva vill bestämma exakt vart vi ska skära kan ni lägga in stödlinjer i originalet som lager, så det blir enkelt för oss att byta ut till skärmärken som fungerar i våra maskiner.
## MÅTT

** För att undvika missförstånd så ange trycksakens mått med breddvärdet först och höjdvärdet sist. Tex. en stående A4-sida anges alltså som 210 x 297 mm. ** WUT STÄMMER DET? MIKA?!

## PAPPER

Kom in och känn och kläm! Eller så kommer vi med förslag om kvalitet, tjocklek och bestrykning. Våra huspapper är Tom&Otto Silk / Gallery Art och Scandia 2000 / Ediction (90g, 130g, 170g, 270g).

## ANTAL

Generellt gäller att större volym ger lägre styckpris, framförallt om trycket görs i offset.

## Några tips:

Packa gärna filerna till en zip-fil innan ni för över dem till oss. Det minskar risken för korrupta filer och snabbar upp överföringen. 

Vår molntjänst är inte något ställe för långvarig lagring. Vi tömmer våra kunders mappar med jämna mellanrum eller när jobbet är slutfört.

Namnge gärna filerna med kund, trycksak och benämning. Tex "God Jul 2017 - Exempelföretaget Vykort".