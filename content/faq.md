+++
title = "FAQ-sida"
lastmod = "2017-10-05"
date = "2017-10-05"
subtitle = "Dicks subtitle"
type = "faq"
+++


# FAQ
Mikos:


## JAG VILL BESTÄLLA EN TRYCKSAK. HUR GÖR JAG?
Våra duktiga grafisk designers hjälper dig att utforma din idé. Kontakta oss så vägleder vi dig hur vi tillsammans får fram den perfekta lösningen som passar dig!

## MIN TRYCKSAK JAG VILL HA FINNS INTE PÅ ER HEMSIDA
Trycksaker kan komma i oändligt många former. PÂ hemsidan förekommer de allra vanligaste produkterna. Har du en unik idé är du välkommen att prata med oss om det.

## JAG HAR EN FÄRDIG DESIGN. VART SKICKAR JAG DEN? ****** /*mÂste vara enkel. FTP?* mejl? sprend? wetransfer?*/  
Helst av allt en länk till filen på din molntjänst. Maila oss så får du en länk till våran molntjänst där du kan ladda upp filer. USB-stickor. Läs mer om materialinlämning <a href=http://192.168.88.101:1313/material>här</a>.

## HUR SNABB KAN VI LEVERERA?
Vårt tryckeri arbetar i världsklass och du får alltid dina trycksaker så snabbt det bara går! Små jobb kan ibland bli klara samma dag. Annars är normal leveranstid fyra arbetsdagar.

## ERBJUDER NI PAKETPRIS?
Självklart blir det billigare om man beställer flera saker. Ring oss och begär en offert.

## KAN NI SKICKA PROVEXEMPLAR?
På tryckeriet har vi visningsexemplar av det mesta som vi kan trycka. Vi har även många av olika papperskvalitéer i lager. Kom gärna förbi för att klämma och känna innan du bestämmer dig för hur din trycksak ska se ut. Vi skickar gärna provtryck med olika papperskvalitéer om så önskas.

## HAR NI EN DISTRIBUTIONSTJÄNST?
Det gör vi! Vi utför adressering och leverans av alla produkter via Postnord och DHL.

_______________________________________________________________________________________________________
# xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
_______________________________________________________________________________________________________

Snott från www.try..cka...kuten.....se/page/faq

## Vad gör jag om jag känner mig osäker när jag ska beställa?

Ingen fråga är för dum, kontakta oss så hjälper vi er med era funderingar. Behoven är väldigt olika och vi guidar dig gärna till en perfekt produkt och lösning som passar dig och din verksamhet.

## Det jag behöver finns inte på er hemsida

Eftersom trycksaker kan produceras i oändliga former och upplagor kan vi inte lägga upp allt på vår hemsida. Däremot kan vi producera er trycksak helt enligt era önskemål. Kontakta oss för offert.

## Vart skickar jag min design/original?

På varje produkt finns ett fält för filuppladdning, alternativt kan ni ladda upp på Hightail Design Dropbox.

Det går även bra att maila filen till asdasdasd@asdasd.se vid beställning via telefon eller mail.

## Jag har ett original men vet inte om det är korrekt

Ingen fara. Välj Datacheck när du beställer så granskar och kvalitetssäkrar våra designers era filer manuellt. Mindre korrigeringar utförs kostnadsfritt och du som kund kan känna dig säker på att ditt original blir optimerat och din trycksak får önskad kvalitet.

## Jag har ingen design eller vet inte hur man skapar ett original

Man behöver inte kunna allt, våra designers hjälper dig ta fram din design i samråd med dig. Kontakta oss för kostnadsförslag.

## Jag har panik och måste få snabb leverans

Kontakta oss innan ni gör er beställning så kan vi oftast hjälpa er med en lösning. Vissa produkter och upplagor tar tyvärr längre tid att producera men oftast hittar vi en lösning som fungerar för er.
Vart vänder jag mig vid fakturafrågor?

Maila easdasdasd@asdasd.se så hjälper vår ekonomiavdelning er.

## Kan man bli återförsäljare?

Kontakta Jan Banan, sälj- och marknadsansvarig, och diskutera era behov.
Ring 070-00000 eller maila Putte.

## Erbjuder ni paketpriser om man handlar flera olika produkter samtidigt?

Ring oss eller maila er förfrågan innan ni gör er beställning så kan vi garanterat ge er ett bra paketpris på era trycksaker.

## Kan ni skicka prover eller referensexemplar?

Givetvis skickar vi prover om ni vill få en uppfattning om produkten innan ni beställer. Kontakta oss så ordnar vi det.

## Kan ni distribuera trycksaken jag trycker hos er?

Det kan vi göra. Vi kan ta fram adressregister, adressera kuvert och lämna in på posten vid behov. Kontakta oss för offert.
<!--todo:
långtext:
    ≈ faq   FIXA COPY
    ≈ miljö     FIXA COPY
    ≈ lämna material  FIXA COPY
    ≈ checklista inför print  FIXA COPY
    [skip] tråkig jävla historia  SKIPPA
kontakt:
    √ google maps    FIXA MARKER
socialt:
    facebook feed
    blog
 -->