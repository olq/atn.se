+++
title = "Produkter och tjänster"
lastmod = "2017-10-05"
date = "2017-10-05"
subtitle = "Det här kan vi göra"
type = "produkter"
efterbehandlingarintro = "Exempel på vad vi kan göra med pappret efter tryckning"
efterbehandlingar = [
  "Bindning",
  "Falsning",
  "Perforering",
  "Hålning",
  "Limning",
  "Klamring",
  "Laminering",
  "och mycket mer...",
]
andraprodukterintro = "Ett par exempel på produkter som inte behöver en egen ruta"
andraprodukter = [
    "Vykort",
    "Fina inbjudningskort",
    "Rapporter",
    "Årsredovisningar",
    "Valsedlar",
    "Gatupratare",
    "med mera..."
]

specialprodukterintro = "Några annorlunda trycksaker som vi också kan göra"
specialprodukter = [
    "Flasketiketter",
    "Kalendrar",
    "Tapeter",
    "Prislappar",
    "med mera..."
]

+++

<!-- 
    Produkterna finns i data/projects/
    1-affischer.yaml
    2-flyers.yaml
    osv...
    Glöm inte att specificera om produkten ska synas på förstasidan eller inte!
 -->
Har du en specialförfrågan, eller behöver du en trycksak som vi inte har listat här?  Hör av dig till oss så ska vi göra vad vi kan för att lösa det!

<!--todo:
långtext:
    ≈ faq   FIXA COPY
    ≈ miljö     FIXA COPY
    ≈ lämna material  FIXA COPY
    ≈ checklista inför print  FIXA COPY
    [skip] tråkig jävla historia  SKIPPA
kontakt:
    √ google maps    FIXA MARKER
socialt:
    facebook feed
    blog
 -->