+++
title = "Miljö"
lastmod = "2017-10-05"
date = "2017-10-05"
subtitle = "Med tryck på miljön!"
type = "miljo"
+++

Vi på Affärstryckeriet strävar hela tiden efter att minska vår belastning på miljön. Vi är Svanencertifierade, REPA-diplomerade och ISO14001 miljöledningscertifierade. Dessutom granskar vi noggrant våra leverantörer och distributörer så att vi alltid arbetar med miljömässigt kvalitativa produkter och tjänster.

<!-- Förutom att dina trycksaker blir så miljövänliga som möjligt kan du som kund hos oss även miljökompenserar för hela din trycksaksproduktion. Hör gärna av dig så berättar vi hur det går till. --> 
<!-- WUT? -->

## SVANENMÄRKT

Bild på svanendiplom

Affärstryckeriet är Svanencertifierade sedan tidigt 90-tal och har sedan dess kontinuerligt följt miljöutvecklingen inom tryckeribranschen och kämpat för att ligga i dess framkant. Svanencertifieringen är ett bevis på att vi försöker minimera vår miljöpåverkan fullt ut, och att våra positiva miljöval styr hela produktens livscykel, från råvara till avfall.

## ISO 14001

Bild på ISO-diplom

År 2011 blev Affärstryckeriet certifierat i miljöledningssystemet ISO 14001. När du trycker hos oss kan du även själv välja att miljökompensera hela din produkt. Hör av dig så berättar vi mer om de olika alternativen.
https://www.sis.se/iso14001/dettariso14001/

## KLIMATKOMPENSATION

I vår strävan efter att påverka miljön så lite som möjligt har du också möjlighet att klimatkompensera för dina trycksaker – inte bara ur tryckperspektiv utan även gällande pappersval. Fråga oss gärna mer om detta när du kontaktar oss för beställning eller offert.
 
## FSC

FSC:s (Forest Stewardship Council) regler värnar om ett hållbart skogsbruk och rättigheter för både växter, djur och människor som finns i skogen. Vi använder oss endast av papper som är godkänt av FCS – vilket innebär att din trycksak går att spåra tillbaka hela vägen till skogen där trädet växte.

## REPA CERTIFIKAT

Bild på repadiplom

<!-- * Bild på svanenlogga*

Utan en levande miljö kan inte mänskligheten som vi känner den överleva just därför är miljön ett ledord som genomsyrar hela vårt företag, miljöaspekterna är en självklarhet. Sverige är ett formidabelt exempel på god skogsvård ända sedan 1903 då man var tvungen att agera kraftfullt för att motverka den enorma utarmning av skogarna som pågick under 1800-talet. Skogsvårdslagen borgar för att en framtid finns för våra skogar genom en god skötsel och ett gott skogsbruk.

Affärstryckeriet försöker i största möjliga mån göra vår belastning av miljön så liten som möjligt. Företaget är sedan 90-talet Svanen Certifierat, vi är REPA diplomerade och vi befinner oss mitt uppe i ISO14001 Miljöledningscertifiering.

Vi är mycket noggranna med att granska våra leverantörer så att även de är starkt miljömedvetna och förmedlar miljömässigt kvalitativa produkter till oss , detsamma gäller för våra distributörer, att de är ”gröna” så långt som det är möjligt.

Kontinuerligt letar vi efter miljövänligare produkter till vår maskinpark som tex. Tvätt/rengöringsmedel, smörjmedel , färg, mm. Vi ser också till att vårt avfall blir omhändertaget på sådant sätt så att miljön skonas som mest.

Vi kan erbjuda dig som kund ytterligare tjänster där du kan satsa än mer på att göra just dina trycksaker så miljövänliga som möjligt. Vi kan erbjuda dig material och arbetsmetoder som gör att du miljökompenserar hela din trycksaksproduktion. Hör gärna av dig så berättar vi hur det går till .

# Svanenmärkt

* Bild på svanendiplom

Affärstryckeriet är Svanencertifierat sedan tidigt 90-tal och sedan dess har företaget kontinuerligt följt miljöutvecklingen inom tryckeribranschen och kämpat för att ligga i dess framkant. Att vara Svanencertifierad borgar för att företaget använder sig av ett helhetsperspektiv där de positiva miljövalen styr en tjänst eller en produkts hela livscykel, från råvara till avfall.

Genom att använda ett Svanencertifierat tryckeri har du kommit en bra bit på väg om du vill påverka miljön positiv riktning. Svanencertifieringen är ett bevis på att det certifierade företaget fullt ut försöker minimera sin miljöpåverkan. Tryck hos oss så trycker du även för miljön.
Läs gärna mer om SVANEN på www.svanen.se

# ISO 14001

* Bild på ISOdiplom

Affärstryckeriet blev den 15/7  -11 certifierat i Miljöledningssystemet ISO 14001.

Vi på ATN arbetar kontinuerligt med att minska vårt miljöslitage och det är också något vi erbjuder dig som kund. Redan det faktum att du trycker hos oss innebär ett bra val för miljön men vi kan också erbjuda dig olika alternativ så att du kan välja nivå på ditt ”miljötänk” från ett ”bra miljöval” till det att du miljökompenserar hela din produkt.

Vill du veta mer är du välkommen att höra av dig, så berättar vi om de olika alternativen.

# Klimatkompensation

I vår strävan efter att bli så lite miljöpåverkande som möjligt arbetar vi även aktivt för att påverka våra kunder och leverantörer att sätta miljön i centrum därför erbjuder vi våra kunder möjligheten att på olika nivåer klimatkompensera sina trycksaker.
Väljer du att trycka på av oss utvalda papper blir din trycksak per automatik klimatkompenserad av oss men om du väljer andra papperstyper kan du då alltså be oss att klimatkompensera ditt pappersval.
Fråga oss gärna mer om detta när du kontaktar oss för beställning eller offert. 

* Stryker allt ordbajs efter det här

# FSC

FCS:s ( Forest Stewardship Council ) regler värnar om ett hållbart skogsbruk och markens framtida förmåga att bära skog. Men reglerna slår även vakt om säkra och sunda arbetsvillkor för dem som arbetar i skogen, urbefolkningars rättigheter samt hotade djur och växter.

Vi har under lång tid till stor del endast använt oss av papper som är godkänt av FSC nu är vi på gång att gå vidare och certifiera oss. Vi är rädda om vår miljö och vill därför också värna om ett långsiktigt och hållbart skogsbruk. Genom märkningen kan vi spåra trycksaken tillbaka genom tryckproduktionen, vidare till pappersbruket, massatillverkaren och slutligen till den skog där träden till trycksaken växte.

Utan skog – inget papper – inget tryckeri – inga trycksaker

# Repa certifikat

* Bild på repadiplom -->